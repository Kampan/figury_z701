import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

public class Panelik extends JPanel {
	
	List<Point> punkty = new ArrayList<>();
	private int maxP;
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		redraw(g);
	}
	
	private void redraw(Graphics g) {
		setBackground(Color.ORANGE);
		
		maxP =  getScale();
		drawLines(g);
		drawCoorSystem(g);
		drawFigure(g);
	}
	
	private int getScale() {
		int maxP=0;
		for(Point point : punkty) {
			maxP = point.y > maxP ? point.y : maxP;
			maxP = point.x > maxP ? point.x : maxP;			
		}
		return maxP;
	}
	
	private void drawLines(Graphics g) {
		g.setColor(Color.GREEN);
		
		int thick = 5;
		g.fillRect(this.getWidth()/2 - thick/2, 0, thick, this.getHeight());
		g.fillRect(0, this.getHeight()/2 - thick/2, this.getWidth(), thick);
	}
	
	private void drawCoorSystem(Graphics g) {
		g.setColor(Color.DARK_GRAY);
		int linesCount = 10;
		
		for(int i=0; i < linesCount; i++) {
			g.drawLine(this.getWidth()/10 * (i +1),0 ,this.getWidth()/10 * (i+1),this.getHeight());
			g.drawLine(0, this.getHeight()/10*(i+1),this.getWidth(), this.getHeight()/10*(i+1));
		}
	}
	
	private void drawFigure(Graphics g) {
		g.setColor(Color.MAGENTA);
		for(int i =0; i<punkty.size(); i++) {
			Point nPoint = null;
			
			if(i== punkty.size()-1) {
				nPoint = punkty.get(0);
			} else {
				nPoint = punkty.get(i+1);
			}
			
			int SCALE_X = (int) Math.round(this.getHeight()/2 - 0.1*this.getHeight()/2);
			int SCALE_Y = (int) Math.round(this.getWidth()/2 - 0.1*this.getWidth()/2);
			int moveFactorX = this.getWidth()/2;
			int moveFactorY = this.getHeight()/2;
			
			
			int xNextPointAfterScale = Math.round(((nPoint.x * SCALE_Y )/ maxP) + moveFactorX);
			int yNextPointAfterScale = Math.round(((nPoint.y * SCALE_X )/ maxP) - moveFactorY) * -1;
			
			int xAfterScale = Math.round(((punkty.get(i).x * SCALE_Y )/ maxP) + moveFactorX);
			int yAfterScale = Math.round(((punkty.get(i).y * SCALE_X )/ maxP) - moveFactorY) * -1;
			
			int pointThickness = 8;
			
			g.fillOval(xAfterScale-pointThickness/2, yAfterScale-pointThickness/2, pointThickness, pointThickness);
			g.drawLine(xAfterScale, yAfterScale, xNextPointAfterScale, yNextPointAfterScale);
		}
	}

	public List<Point> getPoints() {
		return punkty;
	}

	public void setPoints(List<Point> points) {
		this.punkty = points;
	}

}
